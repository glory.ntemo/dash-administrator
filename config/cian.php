<?php

return [

    /*
    |--------------------------------------------------------------------------
    | CONFIGURAÇÕES PRIMAVERA
    |--------------------------------------------------------------------------
    |
     */

    'LOCALJSON' => '../parlamento_ao/src/json/',

    'STORAGEJSON' => 'storage/app/public/json/',
    'APLICACAO_ICON' => 'icons/apple-touch-icon-180x180.png',

    /*
    |--------------------------------------------------------------------------
    | CONFIGURAÇÕES ACTIVE DIRECTORY
    |--------------------------------------------------------------------------
    |
     */
    'utilizaldap' => true,
    'nomeGrupoAdmins' => 'apps.admin',
    'superAdmins' => 'laravel.admin',
];
