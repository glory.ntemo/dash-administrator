import './bootstrap';
import 'livewire-sortable';
import ProgressBar from "progressbar.js";

import Alpine from 'alpinejs';

window.Alpine = Alpine;

Alpine.start();
