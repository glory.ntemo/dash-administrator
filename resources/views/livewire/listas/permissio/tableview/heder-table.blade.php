<tr class="text-uppercase text-sm text-muted">
    <th>Nome</th>
    <th style="width: 30%">Departamento</th>
    <th style="width: 20%">nivel de acesso</th>
    <th class="d-none d-sm-table-cell text-right font-weight-normal" style="width: 8.7%">
        <a class="btn-sm btn-primary pl-3 pr-3" wire:click.prevent="newMember"
           style="cursor: pointer">
            <i class="fa fa-plus"></i> &nbsp;Novo</a>
    </th>
</tr>
