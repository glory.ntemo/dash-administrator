<div class="col-xl-12 mb-12 mb-xxl-12">
    <div class="menu-item">
        <h4 class="menu-content text-muted mb-0 fs-7 text-uppercase">Todos os Itens legislativo</h4>
    </div>
    <div class="card card-flush h-xl-100">

        <div class="card-body pt-1">
            <table id="kt_widget_table_3"
                   class="table table-row-dashed align-middle fs-6 gy-4 my-0 pb-0"
                   data-kt-table-widget-3="all">
                <thead class="d-none">
                <tr>
                    <th>Campaign</th>
                    <th>Platforms</th>
                    <th>Status</th>
                    <th>Team</th>
                    <th>Date</th>
                    <th>Progress</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="min-w-175px">
                        <div class="position-relative ps-6 pe-3 py-2">
                            <div
                                class="position-absolute start-0 top-0 w-4px h-100 rounded-2 bg-info"></div>
                            <a href="#" class="mb-1 text-dark text-hover-primary fw-bolder">Happy
                                Christmas</a>
                            <div class="fs-7 text-muted fw-bolder">Created on 24 Dec 21</div>
                        </div>
                    </td>
                    <td>
                        <!--begin::Icons-->
                        <div class="d-flex gap-2 mb-2">
                            <a href="#">
                                <img src="assets/media/svg/brand-logos/facebook-4.svg"
                                     class="w-20px" alt=""/>
                            </a>
                            <a href="#">
                                <img src="assets/media/svg/brand-logos/twitter-2.svg"
                                     class="w-20px" alt=""/>
                            </a>
                            <a href="#">
                                <img src="assets/media/svg/brand-logos/linkedin-2.svg"
                                     class="w-20px" alt=""/>
                            </a>
                            <a href="#">
                                <img src="assets/media/svg/brand-logos/youtube-3.svg"
                                     class="w-20px" alt=""/>
                            </a>
                        </div>
                        <!--end::Icons-->
                        <div class="fs-7 text-muted fw-bolder">Labor 24 - 35 years</div>
                    </td>
                    <td>
                        <span class="badge badge-light-success">Live Now</span>
                    </td>
                    <td class="min-w-125px">
                        <!--begin::Team members-->
                        <div class="symbol-group symbol-hover mb-1">
                            <!--begin::Member-->
                            <div class="symbol symbol-circle symbol-25px">
                                <img src="assets/media/avatars/300-6.jpg" alt=""/>
                            </div>
                            <!--end::Member-->
                            <!--begin::Member-->
                            <div class="symbol symbol-circle symbol-25px">
                                <img src="assets/media/avatars/300-5.jpg" alt=""/>
                            </div>
                            <!--end::Member-->
                            <!--begin::Member-->
                            <div class="symbol symbol-circle symbol-25px">
                                <img src="assets/media/avatars/300-25.jpg" alt=""/>
                            </div>
                            <!--end::Member-->
                            <!--begin::Member-->
                            <div class="symbol symbol-circle symbol-25px">
                                <img src="assets/media/avatars/300-9.jpg" alt=""/>
                            </div>
                            <!--end::Member-->
                            <!--begin::Member-->
                            <div class="symbol symbol-circle symbol-25px">
                                <div class="symbol-label bg-light-danger">
                                    <span class="fs-7 text-danger">E</span>
                                </div>
                            </div>
                            <!--end::Member-->
                            <!--begin::More members-->
                            <div class="symbol symbol-circle symbol-25px">
                                <div class="symbol-label bg-dark">
                                    <span class="fs-9 text-white">+0</span>
                                </div>
                            </div>
                            <!--end::More members-->
                        </div>
                        <!--end::Team members-->
                        <div class="fs-7 fw-bolder text-muted">Team Members</div>
                    </td>
                    <td class="min-w-150px">
                        <div class="mb-2 fw-bolder">24 Dec 21 - 06 Jan 22</div>
                        <div class="fs-7 fw-bolder text-muted">Date range</div>
                    </td>
                    <td class="d-none">Pending</td>
                    <td class="text-end">
                        <button type="button"
                                class="btn btn-icon btn-sm btn-light btn-active-danger w-25px h-25px ">
                            <!--begin::Svg Icon | path: icons/duotune/arrows/arr001.svg-->
                            <i class="fa fa-trash"></i>
                            <!--end::Svg Icon-->
                        </button>
                    </td>
                </tr>
                <tr>
                    <td class="min-w-175px">
                        <div class="position-relative ps-6 pe-3 py-2">
                            <div
                                class="position-absolute start-0 top-0 w-4px h-100 rounded-2 bg-warning"></div>
                            <a href="#"
                               class="mb-1 text-dark text-hover-primary fw-bolder">Halloween</a>
                            <div class="fs-7 text-muted fw-bolder">Created on 24 Dec 21</div>
                        </div>
                    </td>
                    <td>
                        <!--begin::Icons-->
                        <div class="d-flex gap-2 mb-2">
                            <a href="#">
                                <img src="assets/media/svg/brand-logos/twitter-2.svg"
                                     class="w-20px" alt=""/>
                            </a>
                            <a href="#">
                                <img src="assets/media/svg/brand-logos/instagram-2-1.svg"
                                     class="w-20px" alt=""/>
                            </a>
                            <a href="#">
                                <img src="assets/media/svg/brand-logos/youtube-3.svg"
                                     class="w-20px" alt=""/>
                            </a>
                        </div>
                        <!--end::Icons-->
                        <div class="fs-7 text-muted fw-bolder">Labor 37 - 52 years</div>
                    </td>
                    <td>
                        <span class="badge badge-light-primary">Reviewing</span>
                    </td>
                    <td class="min-w-125px">
                        <!--begin::Team members-->
                        <div class="symbol-group symbol-hover mb-1">
                            <!--begin::Member-->
                            <div class="symbol symbol-circle symbol-25px">
                                <img src="assets/media/avatars/300-1.jpg" alt=""/>
                            </div>
                            <!--end::Member-->
                            <!--begin::Member-->
                            <div class="symbol symbol-circle symbol-25px">
                                <img src="assets/media/avatars/300-25.jpg" alt=""/>
                            </div>
                            <!--end::Member-->
                            <!--begin::Member-->
                            <div class="symbol symbol-circle symbol-25px">
                                <img src="assets/media/avatars/300-6.jpg" alt=""/>
                            </div>
                            <!--end::Member-->
                        </div>
                        <!--end::Team members-->
                        <div class="fs-7 fw-bolder text-muted">Team Members</div>
                    </td>
                    <td class="min-w-150px">
                        <div class="mb-2 fw-bolder">03 Feb 22 - 14 Feb 22</div>
                        <div class="fs-7 fw-bolder text-muted">Date range</div>
                    </td>
                    <td class="d-none">Completed</td>
                    <td class="text-end">
                        <button type="button"
                                class="btn btn-icon btn-sm btn-light btn-active-danger w-25px h-25px ">
                            <!--begin::Svg Icon | path: icons/duotune/arrows/arr001.svg-->
                            <i class="fa fa-trash"></i>
                            <!--end::Svg Icon-->
                        </button>
                    </td>
                </tr>
                <tr>
                    <td class="min-w-175px">
                        <div class="position-relative ps-6 pe-3 py-2">
                            <div
                                class="position-absolute start-0 top-0 w-4px h-100 rounded-2 bg-success"></div>
                            <a href="#" class="mb-1 text-dark text-hover-primary fw-bolder">Cyber
                                Monday</a>
                            <div class="fs-7 text-muted fw-bolder">Created on 24 Dec 21</div>
                        </div>
                    </td>
                    <td>
                        <!--begin::Icons-->
                        <div class="d-flex gap-2 mb-2">
                            <a href="#">
                                <img src="assets/media/svg/brand-logos/facebook-4.svg"
                                     class="w-20px" alt=""/>
                            </a>
                            <a href="#">
                                <img src="assets/media/svg/brand-logos/instagram-2-1.svg"
                                     class="w-20px" alt=""/>
                            </a>
                        </div>
                        <!--end::Icons-->
                        <div class="fs-7 text-muted fw-bolder">Labor 24 - 38 years</div>
                    </td>
                    <td>
                        <span class="badge badge-light-success">Live Now</span>
                    </td>
                    <td class="min-w-125px">
                        <!--begin::Team members-->
                        <div class="symbol-group symbol-hover mb-1">
                            <!--begin::Member-->
                            <div class="symbol symbol-circle symbol-25px">
                                <div class="symbol-label bg-light-danger">
                                    <span class="fs-7 text-danger">M</span>
                                </div>
                            </div>
                            <!--end::Member-->
                            <!--begin::Member-->
                            <div class="symbol symbol-circle symbol-25px">
                                <img src="assets/media/avatars/300-6.jpg" alt=""/>
                            </div>
                            <!--end::Member-->
                            <!--begin::Member-->
                            <div class="symbol symbol-circle symbol-25px">
                                <div class="symbol-label bg-light-primary">
                                    <span class="fs-7 text-primary">N</span>
                                </div>
                            </div>
                            <!--end::Member-->
                            <!--begin::Member-->
                            <div class="symbol symbol-circle symbol-25px">
                                <img src="assets/media/avatars/300-13.jpg" alt=""/>
                            </div>
                            <!--end::Member-->
                        </div>
                        <!--end::Team members-->
                        <div class="fs-7 fw-bolder text-muted">Team Members</div>
                    </td>
                    <td class="min-w-150px">
                        <div class="mb-2 fw-bolder">19 Mar 22 - 04 Apr 22</div>
                        <div class="fs-7 fw-bolder text-muted">Date range</div>
                    </td>
                    <td class="d-none">Pending</td>
                    <td class="text-end">
                        <button type="button"
                                class="btn btn-icon btn-sm btn-light btn-active-danger w-25px h-25px ">
                            <!--begin::Svg Icon | path: icons/duotune/arrows/arr001.svg-->
                            <i class="fa fa-trash"></i>
                            <!--end::Svg Icon-->
                        </button>
                    </td>
                </tr>
                <tr>
                    <td class="min-w-175px">
                        <div class="position-relative ps-6 pe-3 py-2">
                            <div
                                class="position-absolute start-0 top-0 w-4px h-100 rounded-2 bg-danger"></div>
                            <a href="#"
                               class="mb-1 text-dark text-hover-primary fw-bolder">Thanksgiving</a>
                            <div class="fs-7 text-muted fw-bolder">Created on 24 Dec 21</div>
                        </div>
                    </td>
                    <td>
                        <!--begin::Icons-->
                        <div class="d-flex gap-2 mb-2">
                            <a href="#">
                                <img src="assets/media/svg/brand-logos/twitter-2.svg"
                                     class="w-20px" alt=""/>
                            </a>
                            <a href="#">
                                <img src="assets/media/svg/brand-logos/instagram-2-1.svg"
                                     class="w-20px" alt=""/>
                            </a>
                            <a href="#">
                                <img src="assets/media/svg/brand-logos/linkedin-2.svg"
                                     class="w-20px" alt=""/>
                            </a>
                        </div>
                        <!--end::Icons-->
                        <div class="fs-7 text-muted fw-bolder">Labor 24 - 38 years</div>
                    </td>
                    <td>
                        <span class="badge badge-light-warning">Paused</span>
                    </td>
                    <td class="min-w-125px">
                        <!--begin::Team members-->
                        <div class="symbol-group symbol-hover mb-1">
                            <!--begin::Member-->
                            <div class="symbol symbol-circle symbol-25px">
                                <img src="assets/media/avatars/300-6.jpg" alt=""/>
                            </div>
                            <!--end::Member-->
                            <!--begin::Member-->
                            <div class="symbol symbol-circle symbol-25px">
                                <img src="assets/media/avatars/300-25.jpg" alt=""/>
                            </div>
                            <!--end::Member-->
                            <!--begin::Member-->
                            <div class="symbol symbol-circle symbol-25px">
                                <img src="assets/media/avatars/300-1.jpg" alt=""/>
                            </div>
                            <!--end::Member-->
                            <!--begin::Member-->
                            <div class="symbol symbol-circle symbol-25px">
                                <div class="symbol-label bg-light-primary">
                                    <span class="fs-7 text-primary">N</span>
                                </div>
                            </div>
                            <!--end::Member-->
                            <!--begin::Member-->
                            <div class="symbol symbol-circle symbol-25px">
                                <img src="assets/media/avatars/300-5.jpg" alt=""/>
                            </div>
                            <!--end::Member-->
                            <!--begin::More members-->
                            <div class="symbol symbol-circle symbol-25px">
                                <div class="symbol-label bg-dark">
                                    <span class="fs-9 text-white">+0</span>
                                </div>
                            </div>
                            <!--end::More members-->
                        </div>
                        <!--end::Team members-->
                        <div class="fs-7 fw-bolder text-muted">Team Members</div>
                    </td>
                    <td class="min-w-150px">
                        <div class="mb-2 fw-bolder">20 Jun 22 - 30 Jun 22</div>
                        <div class="fs-7 fw-bolder text-muted">Date range</div>
                    </td>
                    <td class="d-none">Pending</td>
                    <td class="text-end">
                        <button type="button"
                                class="btn btn-icon btn-sm btn-light btn-active-danger w-25px h-25px ">
                            <!--begin::Svg Icon | path: icons/duotune/arrows/arr001.svg-->
                            <i class="fa fa-trash"></i>
                            <!--end::Svg Icon-->
                        </button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

    </div>
</div>
