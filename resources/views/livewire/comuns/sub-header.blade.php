<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2 callout callout-success">
            <div class="col-sm-8">
                <h1 class="">
                    <span class="" style="letter-spacing: -1.5px">{{ $titleApplication }}</span> <br>
                    <span class="font-weight-light text-small" style="font-size: 20px">{{ $titleArea }}</span>
                </h1>
            </div>
            <div class="col-sm-4 mt-5">
                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item text-success"><a href="{{route('home')}}">
                            <span class="font-weight-light text-small text-success">Início</span>
                        </a>
                    </li>
                    <li class="breadcrumb-item active">{{ $departament }}</li>
                </ol>
            </div>
        </div>
    </div>
</div>
