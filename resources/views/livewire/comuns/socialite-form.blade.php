<table class="table table-row-dashed align-middle fs-6 gy-4 my-0 pb-0">
    <tbody>
    <tr class="accordion-header d-flex collapsed border-0 p-10 m-0" id="elemento_{{$instance_id}}"
        style="width: 100%!important;">

        <td style=" width: 30%!important;  min-width: 220px!important;">
            <input type="text" class="form-control form-control-sm ">
        </td>
        <td style=" width: 30%!important;  min-width: 220px!important;">
            <input type="text" class="form-control form-control-sm ">
        </td>
        <td style=" width: 30%!important;  min-width: 220px!important;">
            <input type="text" class="form-control form-control-sm ">
        </td>


        <td style="width: 18%!important;" class="text-right justify-content-end p-4">
            <button type="button" data-bs-toggle="collapse"
                    class="btn btn-sm btn-icon btn-light btn-active-light-success toggle h-25px w-100px">
                Guardar
            </button>
            <button type="button" data-bs-toggle="collapse"
                    class="btn btn-sm btn-icon btn-light btn-active-light-danger toggle h-25px w-100px">
                Cancelar
            </button>

        </td>
    </tr>
    </tbody>
</table>
