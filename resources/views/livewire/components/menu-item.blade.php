<div class="menu-item">
    <a class="menu-link" href="{{ __($url) }}">
        <span class="menu-bullet">
            <span class="bullet bullet-dot"></span>
        </span>
        <span class="menu-title">{{ __($title) }}</span>
    </a>
</div>
