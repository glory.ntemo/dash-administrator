{{-- The whole world belongs to you. --}}
<span class="menu-link">
        <livewire:components.svg-icon5/>
        <span class="menu-title">{{ __($title) }}</span>
        <span class="menu-arrow"></span>
</span>

