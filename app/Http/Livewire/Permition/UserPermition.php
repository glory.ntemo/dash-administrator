<?php

namespace App\Http\Livewire\Permition;

use Livewire\Component;

class UserPermition extends Component
{
    public function render()
    {
        return view('livewire.permition.user-permition');
    }
}
