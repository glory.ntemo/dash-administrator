<?php

namespace App\Http\Livewire\Comuns;

use Livewire\Component;

class SearchViews extends Component
{
    public function render()
    {
        return view('livewire.comuns.search-views');
    }
}
