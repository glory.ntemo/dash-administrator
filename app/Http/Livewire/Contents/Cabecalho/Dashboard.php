<?php

namespace App\Http\Livewire\Contents\Cabecalho;

use Livewire\Component;

class Dashboard extends Component
{
    public function render()
    {
        return view('livewire.contents.cabecalho.dashboard');
    }
}
