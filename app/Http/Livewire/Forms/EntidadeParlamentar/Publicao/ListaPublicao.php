<?php

namespace App\Http\Livewire\Forms\EntidadeParlamentar\Publicao;

use Livewire\Component;

class ListaPublicao extends Component
{
    public function render()
    {
        return view('livewire.forms.entidade-parlamentar.publicao.lista-publicao');
    }
}
