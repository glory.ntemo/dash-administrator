<?php

namespace App\Http\Livewire\Forms\EntidadeParlamentar\PerfilDeputado;

use Livewire\Component;

class ListaDeputado extends Component
{
    public function render()
    {
        return view('livewire.forms.entidade-parlamentar.perfil-deputado.lista-deputado');
    }
}
