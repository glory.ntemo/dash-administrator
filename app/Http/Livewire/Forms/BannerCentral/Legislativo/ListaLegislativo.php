<?php

namespace App\Http\Livewire\Forms\BannerCentral\Legislativo;

use Livewire\Component;

class ListaLegislativo extends Component
{
    public function render()
    {
        return view('livewire.forms.banner-central.legislativo.lista-legislativo');
    }
}
