<?php

namespace App\Http\Livewire\Forms\Cabecalho\MenuPrincipal;

use Livewire\Component;

class MenuIntenalTable extends Component
{
    public function render()
    {
        return view('livewire.forms.cabecalho.menu-principal.menu-intenal-table');
    }
}
