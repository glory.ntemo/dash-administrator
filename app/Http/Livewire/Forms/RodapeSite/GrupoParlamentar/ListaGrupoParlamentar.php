<?php

namespace App\Http\Livewire\Forms\RodapeSite\GrupoParlamentar;

use Livewire\Component;

class ListaGrupoParlamentar extends Component
{
    public function render()
    {
        return view('livewire.forms.rodape-site.grupo-parlamentar.lista-grupo-parlamentar');
    }
}
