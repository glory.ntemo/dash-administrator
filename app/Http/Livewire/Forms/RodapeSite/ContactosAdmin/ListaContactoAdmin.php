<?php

namespace App\Http\Livewire\Forms\RodapeSite\ContactosAdmin;

use Livewire\Component;

class ListaContactoAdmin extends Component
{
    public function render()
    {
        return view('livewire.forms.rodape-site.contactos-admin.lista-contacto-admin');
    }
}
