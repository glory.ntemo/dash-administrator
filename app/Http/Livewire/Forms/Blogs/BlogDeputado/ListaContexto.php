<?php

namespace App\Http\Livewire\Forms\Blogs\BlogDeputado;

use Livewire\Component;

class ListaContexto extends Component
{
    public function render()
    {
        return view('livewire.forms.blogs.blog-deputado.lista-contexto');
    }
}
