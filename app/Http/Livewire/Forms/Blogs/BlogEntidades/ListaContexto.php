<?php

namespace App\Http\Livewire\Forms\Blogs\BlogEntidades;

use Livewire\Component;

class ListaContexto extends Component
{
    public function render()
    {
        return view('livewire.forms.blogs.blog-entidades.lista-contexto');
    }
}
