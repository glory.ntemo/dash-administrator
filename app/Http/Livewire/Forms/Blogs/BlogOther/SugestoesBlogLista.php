<?php

namespace App\Http\Livewire\Forms\Blogs\BlogOther;

use Livewire\Component;

class SugestoesBlogLista extends Component
{
    public function render()
    {
        return view('livewire.forms.blogs.blog-other.sugestoes-blog-lista');
    }
}
