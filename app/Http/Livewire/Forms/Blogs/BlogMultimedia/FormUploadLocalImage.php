<?php

namespace App\Http\Livewire\Forms\Blogs\BlogMultimedia;

use Livewire\Component;

class FormUploadLocalImage extends Component
{
    public function render()
    {
        return view('livewire.forms.blogs.blog-multimedia.form-upload-local-image');
    }
}
