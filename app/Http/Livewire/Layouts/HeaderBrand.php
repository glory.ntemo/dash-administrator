<?php

namespace App\Http\Livewire\Layouts;

use Livewire\Component;

class HeaderBrand extends Component
{
    public function render()
    {
        return view('livewire.layouts.header-brand');
    }
}
