<?php

namespace App\Http\Livewire\Components;

use Livewire\Component;

class DataTables extends Component
{
    public function render()
    {
        return view('livewire.components.data-tables');
    }
}
