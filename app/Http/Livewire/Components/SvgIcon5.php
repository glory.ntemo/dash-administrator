<?php

namespace App\Http\Livewire\Components;

use Livewire\Component;

class SvgIcon5 extends Component
{
    public function render()
    {
        return view('livewire.components.svg-icon5');
    }
}
